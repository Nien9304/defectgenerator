TARGET = ../defectGenerator
OBJECT = analyzer.o generator.o main.o common.o config.o struct_spice.o
CC=g++
CCFLAG=-D LINUX_ENV -std=c++0x -O2
TARGET:$(OBJECT)
	$(CC) $(CCFLAG) -o $(TARGET) $(OBJECT)
#makefile knows the dependency between .o .cpp
struct.h: common.h config.h struct_spice.h
	touch struct.h
common.o:common.cpp common.h
	$(CC) $(CCFLAG) -c common.cpp
config.o:config.cpp config.h
	$(CC) $(CCFLAG) -c config.cpp
struct_spice.o:struct_spice.cpp struct_spice.h
	$(CC) $(CCFLAG) -c struct_spice.cpp
	
analyzer.o:analyzer.cpp struct.h
	$(CC) $(CCFLAG) -c analyzer.cpp
generator.o:generator.cpp struct.h
	$(CC) $(CCFLAG) -c generator.cpp
main.o:main.cpp struct.h
	$(CC) $(CCFLAG) -c main.cpp
clean:
	rm -f $(TARGET) $(OBJECT)