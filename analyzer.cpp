#include "struct.h"

void defectGenerator::buildNodeTable(){
	for(unsigned index = 0;	index < m_subckt.m_netlist.size(); ++index){
		//CASE
		switch(m_subckt.m_netlist[index].m_type){
		case ET_R:
			m_nodeTable[m_subckt.m_netlist[index].m_description[1]]
			.m_neighbor.push_back(
				m_subckt.m_netlist[index].m_description[2]);
			m_nodeTable[m_subckt.m_netlist[index].m_description[1]]
			.m_neighborR.push_back(
				index);
			m_nodeTable[m_subckt.m_netlist[index].m_description[2]]
			.m_neighbor.push_back(
				m_subckt.m_netlist[index].m_description[1]);
			m_nodeTable[m_subckt.m_netlist[index].m_description[2]]
			.m_neighborR.push_back(
				index);
			// add layer
			for(unsigned int i = 4; 
			i < m_subckt.m_netlist[index].m_description.size(); ++i){
				string &text = m_subckt.m_netlist[index].m_description[i];
				if(text.find("$layer") == 0){
					string layer = text.substr(text.find_last_of(" =")+1);
					m_nodeTable[m_subckt.m_netlist[index].m_description[1]]
					.m_layer.insert(layer);
					m_nodeTable[m_subckt.m_netlist[index].m_description[2]]
					.m_layer.insert(layer);
					break;
				}
			}
			break;
		case ET_C:
			m_nodeTable[m_subckt.m_netlist[index].m_description[1]];
			m_nodeTable[m_subckt.m_netlist[index].m_description[2]];
			break;
		case ET_D:
			m_nodeTable[m_subckt.m_netlist[index].m_description[1]];
			m_nodeTable[m_subckt.m_netlist[index].m_description[2]];
			break;
		case ET_M:
			m_nodeTable[m_subckt.m_netlist[index].m_description[1]];
			m_nodeTable[m_subckt.m_netlist[index].m_description[2]];
			m_nodeTable[m_subckt.m_netlist[index].m_description[3]];
			m_nodeTable[m_subckt.m_netlist[index].m_description[4]];
			break;
		case ET_STAT:
			break;
		default:
			cerr << "Error: Unknown element type @defectGenerator::buildNodeTable(): " 
				<< ELEMENT_TYPE_NAME[m_subckt.m_netlist[index].m_type] << endl;
			exit(-1);
		}
	}
}
void defectGenerator::traverseNode(node* currentNode, int level){
	if(currentNode->m_group >= 0){
		return;
	}
	currentNode->m_group = m_groupList.size()-1;
	currentNode->m_level = level;
	for(unsigned index = 0; index < currentNode->m_neighbor.size(); ++index){
		if(currentNode->m_level-1 > currentNode->m_neighborNode[index]->m_level){
			if(currentNode->m_neighbor.size() > 2 || level == 0){//a stem or terminal.
				m_groupList.back().m_segment.push_back(segment());
				m_groupList.back().m_segment.back().m_resistor.push_back(
					currentNode->m_neighborR[index]);
				traverseNode(currentNode->m_neighborNode[index], level+1);
			}
			else{
				m_groupList.back().m_segment.back().m_resistor.push_back(
					currentNode->m_neighborR[index]);
				traverseNode(currentNode->m_neighborNode[index], level);
			}
		}
	}
}
void defectGenerator::groupNodes(){
	UDT_MAP<string, node>::iterator nodeIt;
	for(UDT_MAP<string, node>::iterator curNodeIt = m_nodeTable.begin();
	curNodeIt != m_nodeTable.end(); ++curNodeIt){
		curNodeIt->second.m_neighborNode.resize(curNodeIt->second.m_neighbor.size());
		for(unsigned index = 0; index < curNodeIt->second.m_neighbor.size(); ++index){
			nodeIt = m_nodeTable.find(curNodeIt->second.m_neighbor[index]);
			if(nodeIt == m_nodeTable.end()){
				cerr << "Error: node " << curNodeIt->second.m_neighbor[index]
					<< " not found in the node table." << endl;
				cerr << "Ths spice netlist has a floating net." << endl;
				exit(-1);
			}
			curNodeIt->second.m_neighborNode[index] = &nodeIt->second;
		}
	}
	//process ports
	for(vector<string>::iterator port_it = m_portList.begin(); 
	port_it != m_portList.end(); ++port_it){
		//port name as group name
		m_groupList.push_back(group(*port_it)); 
		nodeIt = m_nodeTable.find(*port_it);
		traverseNode(&nodeIt->second, 0);
	}
	//process internal nodes
	for(UDT_MAP<string, node>::iterator nodeIt = m_nodeTable.begin();
	nodeIt != m_nodeTable.end(); ++nodeIt){
		if(nodeIt->second.m_group >= 0) continue;
		//determine the group name
		if(nodeIt->second.m_neighbor.size() == 0){ 
			//single node
			m_groupList.push_back(group(nodeIt->first)); 
		}
		else{
			//node with R, use Rname as group name.
			m_groupList.push_back(group(
				m_subckt.m_netlist[nodeIt->second.m_neighborR[0]].m_description[0])); 
		}
		traverseNode(&nodeIt->second, 0);
	}
	//check all nodes
	for(UDT_MAP<string, node>::iterator nodeIt = m_nodeTable.begin();
	nodeIt != m_nodeTable.end(); ++nodeIt){
		if(nodeIt->second.m_group == -1){
			cerr << "Error: Ungroup nodes or floating nets." << endl;
			cerr << nodeIt->first << endl;
			exit(-1);
		}
	}
}
void defectGenerator::analyzePorts(){
	//process power ports
	for(UDT_SET<string>::iterator port_it = m_powerList.begin(); 
	port_it != m_powerList.end(); ++port_it){
		if(m_nodeTable.find(*port_it) != m_nodeTable.end()){//find power port
			m_PGList.push_back(*port_it);
			m_portList.push_back(*port_it);
		}
	}
	//process ground ports
	for(UDT_SET<string>::iterator port_it = m_groundList.begin(); 
	port_it != m_groundList.end(); ++port_it){
		if(m_nodeTable.find(*port_it) != m_nodeTable.end()){//find ground port
			m_PGList.push_back(*port_it);
			m_portList.push_back(*port_it);
		}
	}
	//process I/O ports
	for(unsigned index = 1; index < m_subckt.m_description.size(); ++index){
			m_IOList.push_back(m_subckt.m_description[index]);
			m_portList.push_back(m_subckt.m_description[index]);
	}
}
void defectGenerator::analyzeDualCell(){
	UDT_SET<string> port_list;
	for(unsigned index = 1; index < m_subckt.m_description.size(); ++index){
		port_list.insert(m_subckt.m_description[index]);
	}
	//process P/G 
	for(vector<string>::iterator port_it = m_PGList.begin(); 
	port_it != m_PGList.end(); ++port_it){
			m_groupList[m_nodeTable[*port_it].m_group].m_groupType = group::GT_PG;
			port_list.erase(*port_it);
	}
	//process ports
	for (UDT_SET<string>::iterator port_it = port_list.begin();
		port_it != port_list.end(); ++port_it) {
		if (port_it->find("L_") == 0) { // L cell
			m_groupList[m_nodeTable[*port_it].m_group].m_groupType = group::GT_LEFT;
		}
		else if (port_it->find("R_") == 0) { // R cell
			m_groupList[m_nodeTable[*port_it].m_group].m_groupType = group::GT_RIGHT;
		}
		else { //unknown(probably single cell)
			cerr << "Unknown port name " << *port_it << endl;
			exit(-1);
			//m_groupList[m_nodeTable[*port_it].m_group].m_groupType = group::GT_SINGLE;
		}
	}
	//propgate all groups using MOS
	int update_count = 1;
	while(update_count != 0){
		update_count = 0;
		for(unsigned i=0; i<m_subckt.m_netlist.size(); ++i){
			if(m_subckt.m_netlist[i].m_type == ET_M){
				int current_group_type = group::GT_UNSET;
				//find current_group_type from existing group type in D/G/S/B
				for(unsigned index=1; index<=4; ++index){
					int &group_type = 
						m_groupList[m_nodeTable[m_subckt.m_netlist[i].
						m_description[index]].m_group].m_groupType;
					if(group_type != group::GT_PG && group_type != group::GT_UNSET){
						if((group_type | current_group_type) == (group::GT_LEFT | group::GT_RIGHT)){
							cerr << 
								"Error: node group from different cells connected together.";
							exit(-1);
						}
						current_group_type = group_type;
					}
				}
				//set group type to D/G/S/B in this MOS
				for(unsigned index=1; index<=4; ++index){
					int &group_type = m_groupList[m_nodeTable[m_subckt.m_netlist[i].
						m_description[index]].m_group].m_groupType;
					if(group_type == 0){
						group_type = current_group_type;
						++update_count;
					}
				}
			}
		}
	}
}