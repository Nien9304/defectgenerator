#include "common.h"

void openFile(ifstream &ifs, const string &filename){
	ifs.open(filename);
	if(ifs.is_open() == false){
		cerr << "Fail to open " << filename << " for reading" << endl; 
		exit(-1); 
	}
}
void openFile(ofstream &ofs, const string &filename){
	ofs.open(filename);
	if(ofs.is_open() == false){
		cerr << "Fail to open " << filename << " for writing" << endl; 
		exit(-1); 
	}
}
void checkId(istream &is, const char *identifier){
	string discard;
	is >> discard;
	if(discard != identifier){
		cerr << "Error: Parse "<< identifier <<" failed. Get:" << discard << endl; 
		exit(-1); 
	}
}
void checkId(istream &is, const string &identifier){
	string discard;
	is >> discard;
	if(discard != identifier){
		cerr << "Error: Parse "<< identifier << " failed. Get:" << discard << endl; 
		exit(-1); 
	}
}
string int2str(int x){
	string str;
	stringstream ss(str);
	ss << x;
	return ss.str();
}