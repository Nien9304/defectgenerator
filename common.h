//
//	common.h
//	common.cpp
//
//	Files: none
//	Structure: none		
//	
//	v1.0.2 //add case insensitive hash and equal class

#ifndef _COMMON_H_
#define _COMMON_H_

#include <iostream>
#include <fstream>
#include <vector>
#include <list>
#include <algorithm>
#include <string>
#include <sstream>

//if your compilier doesn't surpport C11, try -std=c++0x option
//or set #define C11 0
#define C11 1
//definitions
#if C11
	#include <map>
	#include <set>
	#include <unordered_map>
	#include <unordered_set>
	#define UDT_MAP unordered_map
	#define UDT_SET unordered_set
#else
	#include <map>
	#include <set>
	#define UDT_MAP map
	#define UDT_SET set
#endif

using namespace std;

//shared parameters
static const char* INSTANCE_DELIMITER = "#";
static const char* INDENT = "\t";
static const char* SEP = " ";
static const int MAX_PAGE_WIDTH = 72;
static const char LINE_SEP = '-';

//shared types
typedef UDT_MAP<string, int> UDT_STR_INT_MAP;
typedef UDT_MAP<string, string> UDT_STR_STR_MAP;
typedef vector<string> UDT_STR_VEC;
typedef vector<int> UDT_INT_VEC;

//shared functions
void openFile(ifstream &ifs, const string &filename);
void openFile(ofstream &ofs, const string &filename);
void checkId(istream &is, const char *identifier);
void checkId(istream &is, const string &identifier);
string int2str(int x);

//shared class
struct caseInsensitiveHash{
    size_t operator()(const std::string& key) const{
        //You might need a better hash function than this
        size_t h = 0;
				for(size_t pos = 0; pos < key.size(); ++pos){
            h += tolower(key[pos]);
				}
        return h;
    }
};
struct caseInsensitiveEqual{
	bool operator()(const std::string& lhs, const std::string& rhs) const{
		if(lhs.size() != rhs.size()) return false;
		for(size_t pos = 0; pos < lhs.size(); ++pos){
			if(tolower(lhs[pos]) != tolower(rhs[pos])) return false; 
		}
		return true;
	}
};

#endif