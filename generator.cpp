#include "struct.h"

//Rules for defect
//This part is for defect grouping or filtering
bool defectGenerator::is_filteredByLayer(const element &current_element){
	if(m_layerFilter == false) return false;
	set<string> intersection;
	set<string> difference;
	set<string> &s1 = (m_nodeTable[current_element.m_description[1]
	].m_layer);
	set<string> &s2 = (m_nodeTable[current_element.m_description[2]
	].m_layer);
	//skip this defect if 
	//1) they are in different layer: intersection is empty
	//2) the intersection layers are all in black list: 
	//		(intersection - black_list) is empty
	set_intersection(s1.begin(),s1.end(),s2.begin(),s2.end(),
		std::inserter(intersection,intersection.begin()));
	if(intersection.empty()) return true;
	set_difference(intersection.begin(),intersection.end(),
		m_layerBlackList.begin(),m_layerBlackList.end(),
		std::inserter(difference,difference.begin()));
	if(intersection.empty()) return true;
	return false;
}
bool defectGenerator::is_filteredByDual(const element &current_element){
	if(m_dualFilter == false) return false;
	int type1 = m_groupList[m_nodeTable[current_element.m_description[1]].m_group].m_groupType;
	int type2 = m_groupList[m_nodeTable[current_element.m_description[2]].m_group].m_groupType;
	if((type1 | type2) != (group::GT_LEFT | group::GT_RIGHT)){
		return true;
	}
	return false;
}
bool defectGenerator::is_filteredByPG(const element &current_element) {
	int type1 = m_groupList[m_nodeTable[current_element.m_description[1]].m_group].m_groupType;
	int type2 = m_groupList[m_nodeTable[current_element.m_description[2]].m_group].m_groupType;
	if ((type1 & type2) == (group::GT_PG)) {
		return true;
	}
	return false;
}
void defectGenerator::generateBridgeDefect(){
	for(unsigned i=0; i<m_subckt.m_netlist.size(); ++i){
		if(m_subckt.m_netlist[i].m_type == ET_C 
			&& !is_filteredByLayer(m_subckt.m_netlist[i])
			&& !is_filteredByDual(m_subckt.m_netlist[i])
			&& (!is_filteredByPG(m_subckt.m_netlist[i])
				|| !m_filterpg)){
			m_defectList.push_back(defect());
			//add a defect for each C
			m_defectList.back().m_name = m_subckt.m_netlist[i].m_description[0];
			m_defectList.back().m_location.push_back(i);
		}
	}
}
void defectGenerator::generateRandomGroupBridgeDefect(){
	
	UDT_SET<bridge> bridgeTable;
	for(unsigned i=0; i<m_subckt.m_netlist.size(); ++i){
		if(m_subckt.m_netlist[i].m_type == ET_C
			&& !is_filteredByLayer(m_subckt.m_netlist[i])
			&& !is_filteredByDual(m_subckt.m_netlist[i])
			&& (!is_filteredByPG(m_subckt.m_netlist[i])
				|| !m_filterpg)){
			//add one defect for each C between groups
			bridge currentBridge(
				m_nodeTable[m_subckt.m_netlist[i].m_description[1]
				].m_group, 
				m_nodeTable[m_subckt.m_netlist[i].m_description[2]
				].m_group);
			if(!currentBridge.isShort() &&
			bridgeTable.find(currentBridge) == bridgeTable.end()){
				bridgeTable.insert(currentBridge);
				m_defectList.push_back(defect());
				m_defectList.back().m_name = currentBridge.getName(m_groupList);
				m_defectList.back().m_location.push_back(i);
			}
		}
	}
}
void defectGenerator::generateOpenDefect(){
	for(unsigned i=0; i<m_subckt.m_netlist.size(); ++i){
		if(m_subckt.m_netlist[i].m_type == ET_R){
			m_defectList.push_back(defect());
			//add a defect for each R
			m_defectList.back().m_name = m_subckt.m_netlist[i].m_description[0];
			m_defectList.back().m_location.push_back(i);
		}
	}
}
void defectGenerator::generateMedianSegmentOpenDefect(){
	unsigned index;
	for(UDT_GROUP_VEC::iterator groupIt = m_groupList.begin(); 
	groupIt != m_groupList.end(); ++groupIt){
		for(UDT_SEGMENT_VEC::iterator segmentIt = groupIt->m_segment.begin();
		segmentIt != groupIt->m_segment.end(); ++segmentIt){
			index = segmentIt->m_resistor[segmentIt->m_resistor.size()/2]; //median
			m_defectList.push_back(defect());
			//add a defect for each R
			m_defectList.back().m_name = m_subckt.m_netlist[index].m_description[0];
			m_defectList.back().m_location.push_back(index);
		}
	}
}
void defectGenerator::generateTonDefect(){
	for(unsigned i=0; i<m_subckt.m_netlist.size(); ++i){
		if(m_subckt.m_netlist[i].m_type == ET_M){
			m_defectList.push_back(defect());
			//add a defect for each mos
			m_defectList.back().m_name = m_subckt.m_netlist[i].m_description[0];
			m_defectList.back().m_location.push_back(i);
		}
	}
}
void defectGenerator::generateToffDefect(){
	for(unsigned i=0; i<m_subckt.m_netlist.size(); ++i){
		if(m_subckt.m_netlist[i].m_type == ET_M){
			m_defectList.push_back(defect());
			//add a defect for each mos
			m_defectList.back().m_name = m_subckt.m_netlist[i].m_description[0];
			m_defectList.back().m_location.push_back(i);
		}
	}
}
//---------
void defectGenerator::writeDefectFile(ostream &os){
	os << m_cellName << SEP << m_faultName << endl;
	os << "#defect" << SEP << m_defectList.size() << endl;
	for(UDT_DEFECT_VEC::iterator defectIt = m_defectList.begin(); 
	defectIt != m_defectList.end(); ++defectIt){
		os << defectIt->m_name << SEP << defectIt->m_location.size();
		for(UDT_INT_VEC::iterator intIt = defectIt->m_location.begin();
		intIt != defectIt->m_location.end(); ++intIt){
			os << SEP << *intIt;
		}
		os << endl;
	}
	os << "#EOF";
}
