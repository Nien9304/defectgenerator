const char g_version[] = 
//
//	main.cpp
//	DefectGenerator
//
//	Created by PCCO	@2015/04/14
"1.3.21"//fixed dual cell infinte loop
;
#include <fstream>
#include <algorithm>
#include "struct.h"
using namespace std;

int main(int argc, char **argv){
	ofstream ofs;
	ifstream ifs;
	string filename;
	string path;
	string cellName;
	defectGenerator defectGeneratorInst;

	if(argc != 5){
		cerr << "Version: " << g_version << endl << endl;
		cerr << "Usage ./DefectGenerator "
			<< "[working directory(.)] [cell name] [fault name] [config file]" << endl;
		cerr << "Structure of the working directory:" << endl;
		cerr << "./[fault]/info/" << endl;
		cerr << "./info/" << endl;
		cerr << "Files read from the working directory:" << endl;
		cerr << "./info/[cell].spips (spice netlist file)" << endl;
		cerr << "Files write to the working directory:" << endl;
		cerr << "./[fault]/info/[cell].defect (defect location)" << endl;
		exit(-1);
	}
	path = argv[1];
	path += '/';
	cellName = argv[2];
	defectGeneratorInst.m_cellName = cellName;
	defectGeneratorInst.m_faultName = string(argv[3]);
	cout << ">> Read config." << endl;
	filename = argv[4];
	openFile(ifs, filename);
	defectGeneratorInst.setConfig(ifs);
	ifs.close();
	cout << ">> Read cell spice netlist." << endl;
	filename = path + "info/" + cellName + ".spips";
	openFile(ifs, filename);
	defectGeneratorInst.m_subckt.readSerialFile(ifs);
	ifs.close();
	//analyze
	defectGeneratorInst.buildNodeTable();
	defectGeneratorInst.analyzePorts();
	defectGeneratorInst.groupNodes();
	if(defectGeneratorInst.m_faultName == FAULT_TYPE_NAME[FT_DUAL]){
		defectGeneratorInst.analyzeDualCell();
		defectGeneratorInst.m_dualFilter = true;
	}
	/*for(UDT_GROUP_VEC::iterator it = defectGeneratorInst.m_groupList.begin();
		it != defectGeneratorInst.m_groupList.end();++it){
			cout << it->m_name << " " << it->m_groupType << endl;
	}*/
	/*
	transform(defectGeneratorInst.m_defectType.begin(), defectGeneratorInst.m_defectType.end(),
		defectGeneratorInst.m_defectType.begin(), ::tolower);*/
	//only lowercase
	if(defectGeneratorInst.m_faultName == FAULT_TYPE_NAME[FT_BRIDGE] ||
		defectGeneratorInst.m_faultName == FAULT_TYPE_NAME[FT_DUAL]){
		switch(defectGeneratorInst.m_method[FT_BRIDGE]){
		case 0:
			defectGeneratorInst.generateBridgeDefect();
			break;
		case 1:
			defectGeneratorInst.generateRandomGroupBridgeDefect();
			break;
		default:
			cerr << "Error: Unsupport reduction method " 
				<< defectGeneratorInst.m_method[FT_OPEN] << endl;
			exit(-1);
		}
	}
	else if(defectGeneratorInst.m_faultName == FAULT_TYPE_NAME[FT_OPEN]){
		switch(defectGeneratorInst.m_method[FT_OPEN]){
		case 0:
			defectGeneratorInst.generateOpenDefect();
			break;
		case 1:
			defectGeneratorInst.generateMedianSegmentOpenDefect();
			break;
		default:
			cerr << "Error: Unsupport reduction method " 
				<< defectGeneratorInst.m_method[FT_OPEN] << endl;
			exit(-1);
		}
	}
	else if(defectGeneratorInst.m_faultName == FAULT_TYPE_NAME[FT_TON]){
		defectGeneratorInst.generateTonDefect();
	}
	else if(defectGeneratorInst.m_faultName == FAULT_TYPE_NAME[FT_TOFF]){
		defectGeneratorInst.generateToffDefect();
	}
	else{
		cerr << "Error: Unsupport fault type " << defectGeneratorInst.m_faultName << endl;
	}

	cout << ">> Write defect file." << endl;
	filename = path + defectGeneratorInst.m_faultName 
		+ "/info/" + cellName + ".1tfdefect";
	openFile(ofs, filename);
	defectGeneratorInst.writeDefectFile(ofs);
	ofs.close();

	
    return 0;
} 
