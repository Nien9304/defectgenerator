#ifndef _STRUCT_H_
#define _STRUCT_H_

#include "common.h"
#include "config.h"
#include "struct_spice.h"
#include <iterator>

enum FAULT_TYPE{FT_BRIDGE, FT_OPEN, FT_TON, FT_TOFF, FT_DUAL, FT_INVALID};
static const char *FAULT_TYPE_NAME[] = 
	{"bridge", "open", "ton", "toff", "dual", "FT_INVALID"};

struct defect{
	string m_name;
	UDT_INT_VEC m_location;
};
typedef vector<defect> UDT_DEFECT_VEC;
struct node{
	//level must <= -2, loop back(current-target==1) is not allowed.
	node():m_group(-1),m_level(-2){};
	vector<string> m_neighbor; //nodes name connected with R
	//GCC does not support vector of MAP iterator
	vector<node*> m_neighborNode; //nodes ptr connected with R
	vector<unsigned> m_neighborR; //elements ID that connect two nodes
	set<string> m_layer; //layer of neighborR
	int m_group; //group ID map to m_group_list, -1 unset
	int m_level;
};
struct segment{
	//element R in this segment, from one terminal to another.
	vector<unsigned> m_resistor; 
};
typedef vector<segment> UDT_SEGMENT_VEC;
struct group{
	group(const string &name):m_groupType(group::GT_UNSET){
		//node group name extraction method
		size_t percentPos = name.find_last_of('%');
		if(percentPos != string::npos){
			size_t poundPos = name.find_last_of("#:/\\");
			//xRC format for Hspice or DSPF
			//_PM_AOI22LERUX1_DMY%B1#2
			m_name = name.substr(percentPos+1, poundPos-percentPos-1);
		}
		else{
			//other format
			//AA/B_1/2
			//AA/B:2
			size_t poundPos = name.find_last_of("#:/\\");
			m_name = name.substr(0, poundPos);
		}
	};
	static const int GT_UNSET = 0;
	static const int GT_PG = 1;
	static const int GT_LEFT = 2;
	static const int GT_RIGHT = 4;
	static const int GT_SINGLE = 8;
	int m_groupType; //0=unset, 1=P/G, 2=L, 4=R  8=SINGLE
	string m_name;
	UDT_SEGMENT_VEC m_segment;
};
typedef vector<group> UDT_GROUP_VEC;
class bridge{
public:
	bridge(int group1, int group2){
		if(group1 > group2){
			m_group1 = group2;
			m_group2 = group1;
		}
		else{
			m_group1 = group1;
			m_group2 = group2;
		}
		if(group1 < 0){
			cerr << "Error: building bridge on ungrouping nodes." << endl;
			cerr << "Capacitor has nodes that does not appear in resistors." << endl;
			exit(-1);
		}
	};
	string getName(const UDT_GROUP_VEC &groupTable) const{
		return groupTable[m_group1].m_name + "::" + groupTable[m_group2].m_name;
	}
	inline bool isShort() const
    {
		return m_group1 == m_group2;
    }
	/*
	inline operator size_t() const
    {
		size_t seed = (m_group2*16)-m_group1;
        return seed;
    }*/
	inline bool operator==(const bridge &rhs) const
    {
		return (m_group1 == rhs.m_group1 && m_group2 == rhs.m_group2);
    }
	friend class hash<bridge>;
private:
	int m_group1;
	int m_group2;
};
namespace std
{    
   template <>
   struct hash<bridge> : public unary_function<bridge, size_t>
   {
       size_t operator()(const bridge& v) const
       {
			size_t seed = (v.m_group2*16)-v.m_group1;
			return seed;
       }
   };
}
struct defectGenerator{
	defectGenerator():m_dualFilter(false){}
	void setConfig(istream &is){
		m_config.readConfig(is);
		m_config.getCfg(string("spice.power"), m_powerList);
		m_config.getCfg(string("spice.ground"), m_groundList);
		m_config.getCfg(string("defect.layer_filter"), m_layerFilter);
		m_config.getCfg(string("defect.layer_black_list"), m_layerBlackList);
		m_config.getCfg(string("defect.method.bridge"), m_method[FT_BRIDGE]);
		m_config.getCfg(string("defect.method.open"), m_method[FT_OPEN]);
		m_config.getCfg(string("defect.pg_filter"), m_filterpg);
	};
	//netlist analyze
	void analyzePorts(); //get all port names
	void buildNodeTable(); //transform subckt netlist to node
	void groupNodes(); //generate groups and segments
	void analyzeDualCell(); //generate group types in groupList
	void traverseNode(node *currentNode, int level);
	//defect generator functions
	bool is_filteredByLayer(const element &current_element);
	bool is_filteredByDual(const element &current_element);
	bool is_filteredByPG(const element &current_element);
	void generateBridgeDefect();
	void generateOpenDefect();
	void generateTonDefect();
	void generateToffDefect();
	void generateDualDefect();

	void generateRandomGroupBridgeDefect();
	void generateMedianSegmentOpenDefect();
	//IO
	void writeDefectFile(ostream &os);
	//parameter
	subckt m_subckt;
	config m_config;
	UDT_SET<string> m_powerList;
	UDT_SET<string> m_groundList;
	bool m_layerFilter;
	bool m_dualFilter;
	set<string> m_layerBlackList;
	int m_method[FT_INVALID];
	bool m_filterpg;
	//
	string m_cellName;
	string m_faultName;
	vector<string> m_portList;
	vector<string> m_PGList;
	vector<string> m_IOList;
	UDT_DEFECT_VEC m_defectList;
	UDT_MAP<string, node, caseInsensitiveHash, caseInsensitiveEqual> m_nodeTable;
	UDT_GROUP_VEC m_groupList;
};

#endif