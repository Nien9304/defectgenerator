//
//	struct_spice.h
//	struct_spice.cpp
//
//	Files: .spips
//	Structure:
//	-subckt
//		options
//		discription
//		-netlist of element
//			type
//			discription
//			
//	v1.0.5 //fix typos in spips

#ifndef _STRUCT_SPICE_H_
#define _STRUCT_SPICE_H_
#include "common.h"

enum ELEMENT_TYPE{ET_R, ET_C, ET_D, ET_M, ET_X, ET_STAT, ET_INVALID};
static const char *ELEMENT_TYPE_NAME[] = {
	"ET_R", "ET_C", "ET_D", "ET_M", "ET_X", "ET_STAT", "ET_INVALID"};
std::istream& operator >>(std::istream &is, ELEMENT_TYPE &type);

//spice structure
struct element;
typedef vector<element> UDT_ELEMENT_VEC;
struct element{
	element():m_type(ET_INVALID){};
	element(ELEMENT_TYPE type, UDT_STR_VEC discription):m_type(type){
		m_description.swap(discription);
	};

	ELEMENT_TYPE m_type;
	UDT_STR_VEC m_description;
	//R name L R val
	//C name L R val
	//D name L R p/n ...
	//M name D G S B p/n ...
	//X name {pin} subckt
};
struct subckt{
	subckt(){};
	void readSerialFile(ifstream &ifs);
	void writeSerialFile(ostream &os);
	void writeNetlist(ostream &os);
	void writeSpiceFile(ostream &os);

	UDT_STR_VEC m_option;
	UDT_STR_VEC m_description;
	UDT_ELEMENT_VEC m_netlist;
	//.subckt subckt {pin}
};

#endif